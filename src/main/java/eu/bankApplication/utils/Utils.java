package eu.bankApplication.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import eu.bankApplication.model.ResponseBody;

public class Utils {
	
	public static ResponseEntity<ResponseBody> makeStatus(ResponseBody responseBody, HttpStatus status) {
		return new ResponseEntity<ResponseBody>(responseBody, status);
	}

}
