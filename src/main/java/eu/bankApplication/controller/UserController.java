package eu.bankApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.bankApplication.model.ResponseBody;
import eu.bankApplication.model.UserDto;
import eu.bankApplication.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;
	
	@PostMapping
	public ResponseEntity<ResponseBody> addUser(@RequestBody UserDto user) {
		return userService.addUser(user);
	}
	
}
