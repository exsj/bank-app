package eu.bankApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.bankApplication.model.ResponseBody;
import eu.bankApplication.model.TransactionDto;
import eu.bankApplication.service.TransactionService;

@RestController
public class TransactionController {
	
	@Autowired
	TransactionService transactionService;
	
	@PostMapping("/transfer")
	public ResponseEntity<ResponseBody> transfer(@RequestBody TransactionDto transaction) {
		return transactionService.transfer(transaction);
	}
	
	@GetMapping("/report")
	public ResponseEntity<ResponseBody> report(@RequestParam Integer month, @RequestParam Integer year, @RequestParam Long accNo) {
		return transactionService.report(month, year, accNo);
	}

}
