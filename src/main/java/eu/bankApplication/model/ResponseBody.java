package eu.bankApplication.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.bankApplication.entity.Account;
import eu.bankApplication.entity.Transaction;
import eu.bankApplication.entity.User;

@JsonInclude(Include.NON_EMPTY)
public class ResponseBody {
	
	private String message;
	private User user;
	private List<Transaction> listOfTransaction;
	private Account accout;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Transaction> getListOfTransaction() {
		return listOfTransaction;
	}
	public void setListOfTransaction(List<Transaction> listOfTransaction) {
		this.listOfTransaction = listOfTransaction;
	}
	public Account getAccout() {
		return accout;
	}
	public void setAccout(Account accout) {
		this.accout = accout;
	}
	
	public String sanitizeObject() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

	    return mapper.writeValueAsString(this);
	    
	}
	
}
