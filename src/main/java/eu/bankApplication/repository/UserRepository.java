package eu.bankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.bankApplication.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	
}
