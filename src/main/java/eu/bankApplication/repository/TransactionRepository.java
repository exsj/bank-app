package eu.bankApplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.bankApplication.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	
	@Query(value = "select t.* from transaction t where t.transfer_date between  :startDate and :endDate AND (t.to_Acc = :accNo OR t.from_Acc = :accNo)", nativeQuery =true)
	List<Transaction> getTransfersReportInMonth(String startDate, String endDate, Long accNo);
	
}
