package eu.bankApplication.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.bankApplication.entity.Account;
import eu.bankApplication.entity.User;
import eu.bankApplication.model.ResponseBody;
import eu.bankApplication.model.UserDto;
import eu.bankApplication.repository.AccountRepository;
import eu.bankApplication.repository.UserRepository;
import eu.bankApplication.utils.Utils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public ResponseEntity<ResponseBody> addUser(UserDto userDto) {
		ResponseBody responseBody = new ResponseBody();
		
		if (StringUtils.isBlank(userDto.getFirstName())) {
			responseBody.setMessage("First name empty or null");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (StringUtils.isBlank(userDto.getLastName())) {
			responseBody.setMessage("Last name empty or null");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (StringUtils.isBlank(userDto.getEmail())) {
			responseBody.setMessage("Email empty or null");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (StringUtils.isBlank(userDto.getPhone())) {
			responseBody.setMessage("Phone empty or null");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (userDto.getAge() == null) {
			responseBody.setMessage("Age empty or null");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (userDto.getAge() < 16 || userDto.getAge() > 99) {
			responseBody.setMessage("Age must be greater than 16 and lesser than 100");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (!userDto.getPhone().matches("^[0-9]+$")) {
			responseBody.setMessage("Phone number not valid");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (!userDto.getEmail().matches("^(.+)@(.+)$")) {
			responseBody.setMessage("Email not valid");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		User savedUser = new User();
		savedUser.setAge(userDto.getAge());
		savedUser.setEmail(userDto.getEmail());
		savedUser.setFirstName(userDto.getFirstName());
		savedUser.setLastName(userDto.getLastName());
		savedUser.setPhone(userDto.getPhone());
		
		Account account = new Account();
		Account accountDb = accountRepository.save(account);
		
		savedUser.setAccNo(accountDb.getAccNo());
		
		savedUser = userRepository.save(savedUser);
		responseBody.setUser(savedUser);
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);

	}

}
