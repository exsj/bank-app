package eu.bankApplication.service;

import org.springframework.http.ResponseEntity;

import eu.bankApplication.model.ResponseBody;
import eu.bankApplication.model.TransactionDto;

public interface TransactionService {

	ResponseEntity<ResponseBody> transfer(TransactionDto transaction);

	ResponseEntity<ResponseBody> report(Integer month, Integer year, Long accNo);

}
