package eu.bankApplication.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.bankApplication.entity.Account;
import eu.bankApplication.entity.Transaction;
import eu.bankApplication.model.ResponseBody;
import eu.bankApplication.model.TransactionDto;
import eu.bankApplication.repository.AccountRepository;
import eu.bankApplication.repository.TransactionRepository;
import eu.bankApplication.utils.Utils;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public ResponseEntity<ResponseBody> transfer(TransactionDto transactionDto) {
		ResponseBody responseBody = new ResponseBody();
		
		if (transactionDto.getAmount() == null || transactionDto.getAmount() < 1) {
			responseBody.setMessage("Amount cannot be empty, null or 0");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (transactionDto.getFromAcc() == null) {
			responseBody.setMessage("From Account cannot be null or empty");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (transactionDto.getToAcc() == null) {
			responseBody.setMessage("To Account cannot be null or empty");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		Transaction transaction = new Transaction();
		transaction.setAmount(transactionDto.getAmount());
		transaction.setFromAcc(transactionDto.getFromAcc());
		transaction.setToAcc(transactionDto.getToAcc());
		if (StringUtils.isNotBlank(transactionDto.getComments())) {
			transaction.setComments(transactionDto.getComments());
		}
		
		Optional<Account> fromAccOptional = accountRepository.findById(transaction.getFromAcc());
		Account fromAcc = null;
		if (fromAccOptional.isPresent()) {
			fromAcc = fromAccOptional.get();
		} else {
			responseBody.setMessage("From Account doesnt exists");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		Optional<Account> toAccOptional = accountRepository.findById(transaction.getToAcc());
		Account toAcc = null;
		if (toAccOptional.isPresent()) {
			toAcc = toAccOptional.get();
		} else {
			responseBody.setMessage("To Account doesnt exists");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (transaction.getToAcc() == transaction.getFromAcc()) {
			responseBody.setMessage("You cannot transfer to the same account");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		
		if (fromAcc.getBalance() < transaction.getAmount()) {
			responseBody.setMessage("Not enough balance for transfer");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		fromAcc.setBalance(fromAcc.getBalance() - transaction.getAmount());
		toAcc.setBalance(toAcc.getBalance() + transaction.getAmount());
		
		accountRepository.save(fromAcc);
		accountRepository.save(toAcc);
		transaction.setTransferDate(LocalDateTime.now());
		Transaction transactionDb = transactionRepository.save(transaction);
		
		responseBody.setMessage("Transaction succesfully, ID transaction : " + transactionDb.getId());
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseBody> report(Integer month, Integer year, Long accNo) {
		ResponseBody responseBody = new ResponseBody();
		
		if (month == null) {
			responseBody.setMessage("Month cannot be null or empty");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (month < 1 || month > 12) {
			responseBody.setMessage("Month is invalid");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (year == null) {
			responseBody.setMessage("Year cannot be null or empty");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		if (year < 1900 || year > LocalDate.now().getYear()) {
			responseBody.setMessage("Year is invalid");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		
		LocalDateTime startDate = LocalDateTime.of(year, month, 1, 0, 0);
		
		LocalDateTime endDate = startDate.with(TemporalAdjusters.lastDayOfMonth());
		endDate = endDate.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
		
		List<Transaction> listOfTransaction = transactionRepository.getTransfersReportInMonth(startDate.toString(), endDate.toString(), accNo);
		
		responseBody.setListOfTransaction(listOfTransaction);
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}

}
