package eu.bankApplication.service;

import org.springframework.http.ResponseEntity;

import eu.bankApplication.model.ResponseBody;
import eu.bankApplication.model.UserDto;

public interface UserService {

	ResponseEntity<ResponseBody> addUser(UserDto user);

}
